//
//  sensunit.swift
//  sensors
//
//  Created by Josemaria Carazo Abolafia on 10/5/21.
//

import Foundation
import CoreMotion

public class sensunit: ObservableObject {
    
    let motion :CMMotionManager
    var timer :Timer
    
    var c :Double
    
    @Published var mf :[Double]
    @Published var dm :[Double]
    @Published var gs :[Double]
        
    init() {
        motion = CMMotionManager()
        timer = Timer()
        
        c = 0.0
        
        mf = [0,0,0]
        dm = [0,0,0]
        gs = [0,0,0]

    }
    
    public func start() {
        if #available(iOS 10.0, *) {
                startGyros()
            } else {
                // Fallback on earlier versions
            }
            

    }
    
    @available(iOS 10.0, *)
    public func startGyros() {
        var ok1 = false
        var ok2 = false
        var ok0 = false
        let interval = 1.0
        
        if motion.isDeviceMotionAvailable {
            ok0 = true
            self.motion.deviceMotionUpdateInterval = interval
            self.motion.startDeviceMotionUpdates()
            self.dm[0] = c
            self.dm[1] = c
            self.dm[2] = c
        }

        if motion.isGyroAvailable {
            ok1 = true
            self.motion.gyroUpdateInterval = interval
            self.motion.startGyroUpdates()
            self.gs[0] = c
            self.gs[1] = c
            self.gs[2] = c
        // Configure a timer to fetch the accelerometer data.
        }
        
        if motion.isMagnetometerAvailable {
            ok2 = true
            self.motion.magnetometerUpdateInterval = interval
            self.motion.startMagnetometerUpdates()
            self.mf[0] = c
            self.mf[1] = c
            self.mf[2] = c

        }
        
        self.timer = Timer(fire: Date(), interval: (interval),
               repeats: true, block: { (timer) in
           // Get the gyro data.
                if ok0 {
                    if let data = self.motion.deviceMotion {
                        self.dm[0] = round(data.rotationRate.x)
                        self.dm[1] = round(data.rotationRate.y)
                        self.dm[2] = round(data.rotationRate.z)
                    }
                }
                
                if ok1 {
                    if let data = self.motion.gyroData {
                        self.gs[0] = round(data.rotationRate.x)
                        self.gs[1] = round(data.rotationRate.y)
                        self.gs[2] = round(data.rotationRate.z)
                    }
                }
                if ok2 {
                if let data = self.motion.magnetometerData {
                    self.mf[0] = round(data.magneticField.x)
                    self.mf[1] = round(data.magneticField.y)
                    self.mf[2] = round(data.magneticField.z)
                    }
                }
                
                //self.testing()
                
        })
          // Add the timer to the current run loop.
        RunLoop.current.add(self.timer, forMode: .default)

    }
    
    private func testing() {
        print("Magnet Field :  \(mf[0]) · \(mf[1]) · \(mf[2])")
        print("Device Motion:  \(dm[0]) · \(dm[1]) · \(dm[2])")
        print("GyrosScope   :  \(gs[0]) · \(gs[1]) · \(gs[2])")
    }

    public func monitoring() -> String {
        var output = ""
        output += "Magnet Field :  \(mf[0]) · \(mf[1]) · \(mf[2]) \n"
        output += "Device Motion:  \(dm[0]) · \(dm[1]) · \(dm[2]) \n"
        output += "GyrosScope   :  \(gs[0]) · \(gs[1]) · \(gs[2]) \n"
        return output
    }

    @available(iOS 10.0, *)
    public func stopGyros() {
        if self.timer != nil {
            self.timer.invalidate()
            //self.timer = nil

            self.motion.stopGyroUpdates()
        }
    }

}
