//
//  sensorsApp.swift
//  sensors
//
//  Created by Josemaria Carazo Abolafia on 3/5/21.
//

import SwiftUI

@main



struct sensorsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

